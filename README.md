![Image of a teapotahedron](platonics.gif)
# The Six Platonic Solids, in TIC-80

This was a random project to render the six Platonic solids in TIC-80. It contains two parts: a standalone TIC-80 program (`platonics.tic`) and some rust code to dump Wavefront OBJ files into a TIC file.

## The TIC file

Load it into TIC-80 and run it. Press "h" for help; "tab" toggles auto-play mode; ","/"." switch geometry and "l" cycles the rendering mode.

## The rust code

Build with `cargo build`. The syntax is `obj2tic <base.tic> <objfile> <output.tic> - it will load <base.tic> and <objfile>, convert the latter to a TIC sprite chunk and (if required) map chunk, merge those with the base TIC file, and write to <output.tic>. 

## Disclaimers

I wrote this mostly as a joke and I only put the code up because someone wanted to see it; consequently it is not exactly high quality.
