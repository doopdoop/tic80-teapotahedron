mod ticfile;
mod wfobj;

fn serialize_normalized_vertices(data: &mut Vec<u8>, vertices: &Vec<wfobj::WFVector> ) -> std::result::Result<(),String> {
    // NB assumes components between 0.0 and 1.0 inclusive
    let n_vertices = vertices.len();
    data.push( ((n_vertices >> 8) & 0xff) as u8 ); // length hi byte
    data.push( ((n_vertices ) & 0xff) as u8 );     // length lo byte

    // Each normalized vertex coordinate will be in the range 0-1.
    // Scale to the range 0-65535 and then convert to a u16
    // and then split that into two u8s for serialization.
    for v in vertices.iter() {
        for val in vec![v.x, v.y, v.z].into_iter() {
            let v16: u16 = (val*65535.0) as u16;
            let hibyte: u8 = (v16 >> 8) as u8;
            let lobyte: u8 = (v16 & 0xff) as u8;
            data.push(hibyte);
            data.push(lobyte);
        }
    }

    Ok( () )
}



fn serialize_normals(data: &mut Vec<u8>, vertices: &Vec<wfobj::WFVector> ) -> std::result::Result<(),String> {
    // NB assumes components between -1.0 and 1.0 inclusive
    let n_vertices = vertices.len();
    data.push( ((n_vertices >> 8) & 0xff) as u8 ); // length hi byte
    data.push( ((n_vertices ) & 0xff) as u8 );     // length lo byte

    // Scale to the range 0-65535 and then convert to a u16
    // and then split that into two u8s for serialization.
    for v in vertices.iter() {
        for val in vec![v.x, v.y, v.z].into_iter() {
            let vn = f32::max(0.0, f32::min(1.0, 0.5 + 0.5*val ));
            let v16: u16 = (vn*65535.0) as u16;
            let hibyte: u8 = (v16 >> 8) as u8;
            let lobyte: u8 = (v16 & 0xff) as u8;
            data.push(hibyte);
            data.push(lobyte);
        }
    }

    Ok( () )
}

fn bytes_to_chunks(data: Vec<u8>) -> std::result::Result<Vec<ticfile::Chunk>,String> {
    let mut output_chunks = Vec::new();

    // Sprite chunk is mapped from 0x6000 to 0x7fff
    let sprite_chunk_max_len:u16 = 1+(0x7fffu32 - 0x6000u32) as u16;
    // Map chunk is mapped from 0x8000 to 0xff7f
    let map_chunk_max_len:u16 = 1+(0xff7fu32 - 0x8000u32) as u16;

    let max_len = sprite_chunk_max_len + map_chunk_max_len;

    if data.len() > (max_len as usize) {
        return Err(format!("Chunk is too large at {} bytes; max is {} ",data.len(),max_len));
    }

    let sprite_chunk_len: u16 = u16::min(data.len() as u16,sprite_chunk_max_len);
    // Do the sprite chunk
    output_chunks.push( ticfile::Chunk {
        bank_no: 0,
        chunk_type: ticfile::ChunkType::ChunkSprites,
        data_size: sprite_chunk_len,
        data: data[0..(sprite_chunk_len as usize)].to_vec(),
    });

    if (data.len() as u16) > sprite_chunk_len {
        // We need a map chunk as well

        let map_chunk_len: u16 = (data.len() as u16) - sprite_chunk_len;
        output_chunks.push( ticfile::Chunk {
            bank_no: 0,
            chunk_type: ticfile::ChunkType::ChunkMap,
            data_size: map_chunk_len,
            data: data[(sprite_chunk_len as usize)..].to_vec(),
        });
    }

    Ok(output_chunks)
}

fn wfobj_to_chunks(wfobj: &wfobj::WFObj) -> std::result::Result<Vec<ticfile::Chunk>,String> {

    // Split quads into triangles
    let tobj = match wfobj::triangulate(&wfobj) {
        Ok(x) => x,
        Err(x) => {
            return Err(format!("Unable to split into triangles: {}",x));
        }
    };
    // Normalize so 0 <= x,y,z <= 1
    let nobj = match wfobj::normalize01(&tobj) {
        Ok(x) => x,
        Err(x) => {
            return Err(format!("Unable to normalize vertices: {}",x));
        }
    };

    eprintln!("Generated {} triangles",nobj.faces.len());

    let mut data: Vec<u8> = Vec::new();

    // Process vertex data
    serialize_normalized_vertices(&mut data, &nobj.vertices)?;

    // Process face data
    let n_faces = nobj.faces.len();
    data.push( ((n_faces>> 8) & 0xff) as u8 ); // length hi byte
    data.push( ((n_faces) & 0xff) as u8 );     // length lo byte

    for face in nobj.faces.iter() {
        if 3!=face.indices.len() {
            return Err(format!("Face with {} vertices should have 3",face.indices.len()));
        }
        for ix in face.indices.iter() {
            data.push( ((ix>> 8) & 0xff) as u8 ); // hi byte
            data.push( (ix & 0xff) as u8 );        //lo byte
        }

        if 3!=face.normals.len() {
            return Err(format!("Face with {} normal indices hould have 3",face.normals.len()));
        }
        for ix in face.normals.iter() {
            data.push( ((ix>> 8) & 0xff) as u8 ); // hi byte
            data.push( (ix & 0xff) as u8 );        //lo byte
        }
    }
    // Process normal data
    serialize_normals(&mut data, &nobj.normals)?;

    // Convert to TIC-80 chunks 

    let output_chunks = bytes_to_chunks(data)?;
    Ok(output_chunks)

}

fn merge_chunks(chunks: &Vec<ticfile::Chunk>, to_merge: &Vec<ticfile::Chunk>) -> 
    std::result::Result<Vec<ticfile::Chunk>,String> {

    // Take a list of chunks, and a list to merge with it.
    // Drop any chunks from the first list which share a type with any in the second list,
    // so that we don't have eg duplicate sprite chunks.
    // Return a list of merged chunks.

    let mut merge_types: std::collections::HashSet<ticfile::ChunkType> = std::collections::HashSet::new();

    for chunk in to_merge.iter() {
        if merge_types.contains(&chunk.chunk_type) {
            return Err(format!("Chunks to merge contain repeated type: {}",chunk.chunk_type).to_string());
        }
        merge_types.insert(chunk.chunk_type);
    }

    let mut output_chunks: Vec<ticfile::Chunk> = Vec::new();

    // Include each base chunk in output unless it has the same type as any of the
    // to_merge chunks
    for chunk in chunks.iter() {
        if merge_types.contains(&chunk.chunk_type) {
            eprintln!("WARNING - dropping existing chunk of type {}",chunk.chunk_type);
        } else {
            output_chunks.push(chunk.clone());
        }
    }
    // Merge the new chunks
    for chunk in to_merge.iter() {
        output_chunks.push(chunk.clone());
    }

    Ok( output_chunks )
}

fn main()  {
    let args: Vec<String> = std::env::args().collect();

    if 4 != args.len() {
        eprintln!("Usage: {} <infilename> <objfilename> <outfilename>",args[0]);
        std::process::exit(-1);
    }

    let infilename = &args[1];
    let objfilename = &args[2];
    let outfilename = &args[3];

    // Read the TIC file
    let chunks = match ticfile::read_ticfile(infilename) {
        Ok(chunks) => chunks,
        Err(err) => {
            eprintln!("Unable to read {}: {}",infilename,err);
            std::process::exit(-1);
        }
    };

    // Read the OBJ file
    let obj = match wfobj::read_wfobjfile(objfilename) {
        Ok(obj) => obj,
        Err(err) => {
            eprintln!("Unable to read {}: {}",objfilename,err);
            std::process::exit(-1);
        }
    };

    // Make sure it's internally consistent
    if let Err(msg) = wfobj::check_wfobj(&obj) {
        eprintln!("Failed consistency check! {}",msg);
        std::process::exit(-1);
    }

    // Create new sprite chunk from the OBJ data
    let new_chunks = match wfobj_to_chunks(&obj) {
        Ok(x) => x,
        Err(x) => {
            eprintln!("Unable to generate sprite chunk: {}",x);
            std::process::exit(-1);
        }
    };

    let output_chunks = match merge_chunks(&chunks,&new_chunks) {
        Ok(x) => x,
        Err(x) => {
            eprintln!("Unable to merge chunks: {}",x);
            std::process::exit(-1);
        }
    };

    // Write the new TIC file
    if let Err(err) = ticfile::write_ticfile(&output_chunks, outfilename) {
        eprintln!("Unable to write {}: {}",outfilename,err);
        std::process::exit(-1);
    }

    eprintln!("Done!");


}
