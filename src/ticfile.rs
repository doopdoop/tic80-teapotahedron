use std::io::{Read,Write};
// see https://github.com/nesbox/TIC-80/wiki/.tic-File-Format
#[derive(Clone)]
pub struct Chunk {
    pub bank_no: u8,
    pub chunk_type: ChunkType,
    pub data_size: u16,
    pub data: Vec<u8>,
}

#[repr(u8)]
#[derive(Clone,Copy,PartialEq,Eq,Hash)]
pub enum ChunkType {
    ChunkTiles = 1,
    ChunkSprites = 2,
    ChunkCoverDep = 3,
    ChunkMap = 4,
    ChunkCode = 5,
    ChunkFlags = 6,
    ChunkSamples = 9,
    ChunkWaveform = 10,
    ChunkPalette = 12,
    ChunkPatternsDep = 13,
    ChunkMusic = 14,
    ChunkPatterns = 15,
    ChunkCodeZip = 16,
    ChunkDefault = 17,
    ChunkScreen = 18,
    ChunkBinary = 19,
    ChunkReserved(u8),
}
impl ChunkType {
    fn from_u8(value: u8) -> Self {
        match value {
            1 => ChunkType::ChunkTiles,
            2 => ChunkType::ChunkSprites,
            3 => ChunkType::ChunkCoverDep,
            4 => ChunkType::ChunkMap,
            5 => ChunkType::ChunkCode,
            6 => ChunkType::ChunkFlags,
            9 => ChunkType::ChunkSamples,
            10 => ChunkType::ChunkWaveform,
            12 => ChunkType::ChunkPalette,
            13 => ChunkType::ChunkPatternsDep,
            14 => ChunkType::ChunkMusic,
            15 => ChunkType::ChunkPatterns,
            16 => ChunkType::ChunkCodeZip,
            17 => ChunkType::ChunkDefault,
            18 => ChunkType::ChunkScreen,
            19 => ChunkType::ChunkBinary,
        _ => ChunkType::ChunkReserved(value),
        }
    }

    fn as_u8(&self) -> u8 {
        match self {
            &ChunkType::ChunkTiles => 1,
            &ChunkType::ChunkSprites => 2,
            &ChunkType::ChunkCoverDep => 3,
            &ChunkType::ChunkMap => 4,
            &ChunkType::ChunkCode => 5,
            &ChunkType::ChunkFlags => 6,
            &ChunkType::ChunkSamples => 9,
            &ChunkType::ChunkWaveform => 10,
            &ChunkType::ChunkPalette => 12,
            &ChunkType::ChunkPatternsDep => 13,
            &ChunkType::ChunkMusic => 14,
            &ChunkType::ChunkPatterns => 15,
            &ChunkType::ChunkCodeZip => 16,
            &ChunkType::ChunkDefault => 17,
            &ChunkType::ChunkScreen => 18,
            &ChunkType::ChunkBinary => 19,
            &ChunkType::ChunkReserved(x) => x,
        }
    }
}

impl std::fmt::Display for ChunkType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ChunkType::ChunkTiles => write!(f,"ChunkTiles"),
            ChunkType::ChunkSprites => write!(f,"ChunkSprites"),
            ChunkType::ChunkCoverDep => write!(f,"ChunkCoverDep"),
            ChunkType::ChunkMap => write!(f,"ChunkMap"),
            ChunkType::ChunkCode => write!(f,"ChunkCode"),
            ChunkType::ChunkFlags => write!(f,"ChunkFlags"),
            ChunkType::ChunkSamples => write!(f,"ChunkSamples"),
            ChunkType::ChunkWaveform => write!(f,"ChunkWaveform"),
            ChunkType::ChunkPalette => write!(f,"ChunkPalette"),
            ChunkType::ChunkPatternsDep => write!(f,"ChunkPatternsDep"),
            ChunkType::ChunkMusic => write!(f,"ChunkMusic"),
            ChunkType::ChunkPatterns => write!(f,"ChunkPatterns"),
            ChunkType::ChunkCodeZip => write!(f,"ChunkCodeZip"),
            ChunkType::ChunkDefault => write!(f,"ChunkDefault"),
            ChunkType::ChunkScreen => write!(f,"ChunkScreen"),
            ChunkType::ChunkBinary => write!(f,"ChunkBinary"),
            ChunkType::ChunkReserved(x) => write!(f,"ChunkReserved({})",x),
        }
    }
}

fn read_chunk(file: &mut std::fs::File) -> std::io::Result<Option<Chunk>> {
    let mut first_byte = [0u8; 1];
    let mut header = vec![0; 3];

    if let Err(_) = file.read_exact(&mut first_byte) {
        // If the first read fails then assume we've got to the end of the
        // file, and just return None.
        return Ok(None);
    }

    file.read_exact(&mut header)?;
    let bank_no: u8 = first_byte[0] >> 5;
    let chunk_type: ChunkType = ChunkType::from_u8(first_byte[0] & 0x1f);
    let data_size = u16::from_le_bytes([header[0],header[1]]);
    let mut data = vec![0; data_size as usize];
    file.read_exact(&mut data)?;

    eprintln!("Read chunk: bankno {} ctype {} size {}",bank_no, &chunk_type, data_size);
    let chunk = Chunk {
        bank_no: bank_no,
        chunk_type: chunk_type,
        data_size: data_size,
        data: data
    };

    Ok(Some(chunk))

}

pub fn read_ticfile(filename: &String) -> std::io::Result<Vec<Chunk>> {
    let mut file = std::fs::File::open(&filename)?;

    let mut chunks: Vec<Chunk> = Vec::new();

    loop {
        match read_chunk(&mut file) {
            Ok(chunkopt) => {
                match chunkopt {
                    Some(chunk) => {
                        chunks.push(chunk);
                    },
                    None => { break; }
                }
            }
            Err(err) => {
                return Err(err);
            }
        }
    }
    eprintln!("Loaded {}",filename);
    return Ok(chunks);

}

fn write_chunk(chunk: &Chunk, file: &mut std::fs::File) -> std::io::Result<()> {

    let first_byte = (chunk.bank_no << 5) | (chunk.chunk_type.as_u8() & 0x1f);
    file.write_all(&[first_byte]) ?;

    let length_desc: [u8; 2] = chunk.data_size.to_le_bytes();
    file.write_all(&length_desc)?;

    let unused_byte: u8 = 0;
    file.write_all(&[unused_byte])?;

    file.write_all(&chunk.data)?;

    return Ok( () );
}

pub fn write_ticfile(chunks: &Vec<Chunk>, filename: &String) -> std::io::Result<()> {
    let mut file = std::fs::File::create(&filename) ?;
    for chunk in chunks.iter() {
        write_chunk(&chunk, &mut file)?;
        eprintln!("Wrote {} chunk",chunk.chunk_type);
    }

    return Ok( () );
}
