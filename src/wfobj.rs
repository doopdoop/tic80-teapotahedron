use std::io::BufRead;

#[derive(Clone)]
pub struct WFObj {
    pub vertices: Vec<WFVector>,
    pub faces: Vec<WFFace>,
    pub normals: Vec<WFVector>,
}

#[derive(Clone)]
pub struct WFVector {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

pub struct WFFace {
    pub indices: Vec<i32>,
    pub normals: Vec<i32>,
}

impl Clone for WFFace {
    fn clone(&self) -> WFFace {
        WFFace { indices: self.indices.to_vec(), normals: self.normals.to_vec(), }
    }
}

#[allow(dead_code)] // debugging function
pub fn dump_wfobj(wfobj: &WFObj) {
    for vertex in wfobj.vertices.iter() {
        println!("Vertex {} {} {}",vertex.x,vertex.y,vertex.z);
    }
    for face in wfobj.faces.iter() {
        print!("Face");
        for ix in face.indices.iter() {
            print!(" {}",ix);
        }
        print!("\n");
    }
}

pub fn triangulate(wfobj: &WFObj) -> std::result::Result<WFObj,String> {
    // Go through all faces and split quads into two triangles.
    // Return an error if anything is encountered that's neither a quad
    // nor a triangle.
    let mut new_faces: Vec<WFFace> = Vec::new();
    for face in wfobj.faces.iter() {
        match face.indices.len() {
            3 => {
                new_faces.push(face.clone());
            },
            4 => {
                //  0---3     0---3        3
                //  |   |     |  /       / |
                //  |   | ==> | /   +   /  |
                //  1---2     1        1---2
                new_faces.push(WFFace{
                    indices: vec![
                        face.indices[0],
                        face.indices[1],
                        face.indices[3],
                    ],
                    normals: vec![
                        face.normals[0],
                        face.normals[1],
                        face.normals[3],
                    ],
                });
                new_faces.push(WFFace{
                    indices: vec![
                        face.indices[1],
                        face.indices[2],
                        face.indices[3],
                    ],
                    normals: vec![
                        face.normals[1],
                        face.normals[2],
                        face.normals[3],
                    ],
                });
            },
            x => {
                return Err(format!("Incorrect number of faces {} in face - should be 3 or 4",x).to_string())
            },
        }
    }
    let newobj = WFObj {
        vertices: wfobj.vertices.clone(),
        faces: new_faces,
        normals: wfobj.normals.clone(),
    };
    Ok(newobj)
}

pub fn normalize01(wfobj: &WFObj) -> std::result::Result<WFObj,String> {
    // Normalize all vertices to be in the range 0.0 <= x <= 1.0
    // Use same max/min for all dimensions so that relative measurements are preserved
    if 0==wfobj.vertices.len() {
        return Err("Cannot normalize empty object".to_string());
    }
    let v0 = &wfobj.vertices[0];
    let mut vmax= v0.x;

    for v in wfobj.vertices.iter() {
        if v.x > vmax { vmax = v.x; }
        if (-v.x) > vmax { vmax = -v.x; }
        if v.y > vmax { vmax = v.y; }
        if (-v.y) > vmax { vmax = -v.y; }
        if v.z > vmax { vmax = v.z; }
        if (-v.z) > vmax { vmax = -v.z; }
    }

    let mut newverts: Vec<WFVector> = Vec::new();

    let vmin = -vmax;
    for v in wfobj.vertices.iter() {
        let v2 = WFVector {
            x: (v.x-vmin)/(vmax-vmin),
            y: (v.y-vmin)/(vmax-vmin),
            z: (v.z-vmin)/(vmax-vmin),
        };
        newverts.push(v2);
    }


    let newobj = WFObj {
        vertices: newverts,
        faces: wfobj.faces.to_vec(),
        normals: wfobj.normals.clone(),
    };
    Ok(newobj)
}

pub fn check_wfobj(wfobj: &WFObj) -> std::result::Result<(),String> {
    let n_vertices = wfobj.vertices.len();
    let n_normals = wfobj.normals.len();
    let mut faceno = 0;
    for face in wfobj.faces.iter() {
        for &ix in face.indices.iter() {
            if 0==ix {
                return Err(format!("Zero index in face {}",faceno));
            }
            if (ix as usize) > n_vertices {
                return Err(format!("Bad index {} in face {}",ix,faceno));
            }
        }
        for &ix in face.normals.iter() {
            if 0==ix {
                return Err(format!("Zero normal index in face {}",faceno));
            }
            if (ix as usize) > n_normals {
                return Err(format!("Bad normal index {} in face {}",ix,faceno));
            }
        }
        faceno = faceno + 1;
    }

    Ok(())
}


fn make_error(msg: &str) -> std::io::Error {
    std::io::Error::new( std::io::ErrorKind::Other, msg )
}


fn read_vector(words: &Vec<String>) -> std::io::Result<WFVector> {
    if 4!=words.len() {
        return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Expected 4-word line for vector but got {} words",words.len())
        ));
    }

    let x = words[1].parse::<f32>();
    let y = words[2].parse::<f32>();
    let z = words[3].parse::<f32>();

    match (x, y, z) {
        (Ok(x), Ok(y), Ok(z)) => {
            Ok( WFVector { x, y, z } )
        },
        _ => {
            Err( make_error("Unable to parse x/y/z") )
        },
    }

}

fn read_face(words: &Vec<String>) -> std::io::Result<WFFace> {
    let mut indices: Vec<i32> = Vec::new();
    let mut normals: Vec<i32> = Vec::new();

    for word in words.iter().skip(1) {
        // each word will be something like aaa/bbb/ccc for integer aaa, bbb, ccc
        let els: Vec<String> = word.split("/").map(|s| s.to_string()).collect();
        if els.is_empty() {
            return Err( make_error("Unable to parse vertex index for face") );
        }
        let ix = els[0].parse::<i32>();

        match ix {
            Ok(ix) => {
                indices.push(ix);
            },
            Err(_) => {
                return Err( make_error("Bad vertex index") );
            },
        }
        if els.len() >= 3 {
            let nix = els[2].parse::<i32>();

            match nix {
                Ok(nix) => {
                    normals.push(nix);
                },
                Err(_) => {
                    return Err( make_error("Bad normal index") );
                },
            }
        }
    }

    let face = WFFace { indices, normals };
    Ok(face)
}

pub fn read_wfobjfile(filename: &String) -> std::io::Result<WFObj> {
    let file = std::fs::File::open(filename)?;
    let reader = std::io::BufReader::new(file);

    let mut vertices: Vec<WFVector> = Vec::new();
    let mut faces: Vec<WFFace> = Vec::new();
    let mut normals: Vec<WFVector> = Vec::new();

    for lineres in reader.lines() {
        let line = lineres?; // handle any errors in reading
        let words: Vec<String> = line.split_whitespace().map(|s| s.to_string()).collect();
        if words.is_empty() { continue; }

        match words[0].as_str() {
            "v" => {
                // read vector
                let vec = read_vector(&words)?;
                vertices.push(vec);
            },
            "vn" => {
                // read normal "vector" (ok pseudovector)
                let vec = read_vector(&words)?;
                normals.push(vec);
            },
            "f" => {
                // read face
                let face = read_face(&words)?;
                faces.push(face);
            },
            _ => {}, // do nothing otherwise
        }

    }
    let wfobj = WFObj { vertices, faces, normals };
    eprintln!("Loaded {}",filename);
    eprintln!("{} vertices {} faces {} normals ",wfobj.vertices.len(),wfobj.faces.len(),wfobj.normals.len());
    Ok(wfobj)
}

